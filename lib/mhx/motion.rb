require "yaml"

module MHX
  class Motion
    YAML_DATA_MOTIONS = {}
    MOTIONS = {}

    # それぞれのmotionはsingleton
    def self.find(motion_str, weapon_class)
      data = load_data(weapon_class)
      data.each do |d|
        if d["name"] =~ /#{motion_str}/
          return MOTIONS[weapon_class][d["name"]] ||= self.new(d, weapon_class)
        end
      end
      fail "no motion #{motion_str} found"
    end

    def self.load_data(weapon_class)
      return YAML_DATA_MOTIONS[weapon_class] if YAML_DATA_MOTIONS[weapon_class]
      MOTIONS[weapon_class] = {}

      # TODO: きれいに
      if weapon_class == MHX::SwordAndShield
        filename = "sword_and_shield.yml"
      elsif weapon_class == MHX::SlashAxe
        filename = "slash_axe.yml"
      elsif weapon_class == MHX::ChargeAxe
        filename = "charge_axe.yml"
      end

      YAML_DATA_MOTIONS[weapon_class] = YAML.load(File.read(File.join(File.dirname(__FILE__), "..", "..", "data", "motions", filename)))
    end

    attr_reader :name, :atk, :options, :weapon_class

    def initialize(data, weapon_class)
      @name = data["name"]
      @atk = data["atk"]
      @options = data["options"] || {}
      @weapon_class = weapon_class
      self
    end
  end
end
