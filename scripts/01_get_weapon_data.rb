require "capybara/poltergeist"
require "yaml"
require "pry"

Capybara.run_server = false
Capybara.register_driver :poltergeist do |app|
    d = Capybara::Poltergeist::Driver.new(
      app,
      :js_errors => false,
      :phantomjs_options => [
        '--ignore-ssl-errors=yes',
      ]
    )
    d.headers = { 'User-Agent' => 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Mobile/9A334 Safari/7534.48.3' }
    d
end

@session = Capybara::Session.new(:poltergeist)
@session.visit "http://wiki.mhxg.org/data/2708.html"

headers = []
@session.find_all('thead tr[role="row"]').each do |tr|
  ths = tr.find_all("th")
  ths.each do |th|
    headers << th.text
  end
end

def parse_td(td, header)
  case header
  when "斬れ味"
    # 斬れ味Hashを返す
    # { #匠Lv    緋  赤  黄  緑  青  白  紫  黒
    #    0   => [10, 10, 10, 10, 10, 10, 10, 0]
    #    2   => [10, 10, 10, 10, 10, 10, 10, 0]
    # }
    takumis = [0, 2]
    takumi_index = 0
    kr = 0
    data = {}
    td.find_all("span").each do |span|
      next unless span[:class] =~ /^kr/
      span[:class] =~ /^kr(\d+)$/
      kr_new = $1.to_i
      takumi_index += 1 if kr_new < kr
      data[takumis[takumi_index]] ||= Array.new(8, 0)
      data[takumis[takumi_index]][kr_new] = span.text.size
      kr = kr_new
    end
    data

  when "スロット"
    3 - td.text.count("-")

  when "性能"
    # td.text sample: "会心-20% 防御+15 雷48"
    # returns: {
    #   "会心": -20,
    #   "防御": 15,
    #   "雷": 48,
    # }
    td.text.split(" ").each_with_object({}) do |text, obj|
      text =~ /^([^0-9\-+]+)([\-|+]*\d+\%*)$/
      next unless $1
      obj[$1] = $2.to_i
    end

  when "武器名"
    # 末尾の数字を除去
    td.text =~ /^(.*?)(\d+)$/
    $1

  else
    if td.text =~ /^\d+$/
      td.text.to_i
    else
      td.text
    end

  end
end

data = []
@session.find_all('tbody tr[role="row"]').each do |tr|
  tds = tr.find_all("td")
  data << headers.each_with_object({}).with_index do |(h, obj), i|
    obj[h] = parse_td(tds[i], h)
  end
end

puts data.to_yaml
