require "spec_helper"
require "./lib/mhx"

describe MHX::SlashAxe do
  describe ".all" do
    subject { MHX::SlashAxe.all }

    it "is array of MHX::SlashAxe instance" do
      expect(subject.class).to eq(Array)
      expect(subject.map{|sa| sa.class}.uniq).to eq([MHX::SlashAxe])
    end
  end

  describe ".new" do
    subject { MHX::SlashAxe.new(name, skill_kireaji_lv: skill_kireaji_lv) }
    let(:skill_kireaji_lv) { 0 }

    context "レギオン、匠0のとき" do
      let(:name) { "レギオン" }

      it "has attributes" do
        expect(subject.name).to eq("叛逆斧バラクレギオン")
        expect(subject.atk).to eq(180)
        expect(subject.options).to eq({"会心" => 20})
        expect(subject.kireaji_gauge).to eq({0=>[6, 3, 15, 10, 3, 3, 0, 0], 2=>[6, 3, 15, 10, 3, 3, 0, 0]})
        expect(subject.slots).to eq(2)
        expect(subject.skill_kireaji_lv).to eq(0)
        expect(subject.kireaji).to eq(400)
        expect(subject.bottle).to eq("強撃ビン")
      end
    end

    context "レウス斧、匠0のとき" do
      let(:name) { "リオエクスレイズ" }

      it "has attributes" do
        expect(subject.name).to eq("焔斧リオエクスレイズ")
        expect(subject.atk).to eq(190)
        expect(subject.options).to eq({"会心" => 5, "火" => 35})
        expect(subject.kireaji_gauge).to eq({0=>[17, 2, 7, 6, 3, 0, 0, 5], 2=>[17, 2, 7, 6, 7, 1, 0, 0]})
        expect(subject.slots).to eq(1)
        expect(subject.skill_kireaji_lv).to eq(0)
        expect(subject.kireaji).to eq(350)
        expect(subject.bottle).to eq("強撃ビン")
      end
    end

    context "レウス斧、匠2のとき" do
      let(:name) { "リオエクスレイズ" }
      let(:skill_kireaji_lv) { 2 }

      it "has attributes" do
        expect(subject.name).to eq("焔斧リオエクスレイズ")
        expect(subject.atk).to eq(190)
        expect(subject.options).to eq({"会心" => 5, "火" => 35})
        expect(subject.kireaji_gauge).to eq({0=>[17, 2, 7, 6, 3, 0, 0, 5], 2=>[17, 2, 7, 6, 7, 1, 0, 0]})
        expect(subject.slots).to eq(1)
        expect(subject.skill_kireaji_lv).to eq(2)
        expect(subject.kireaji).to eq(400)
        expect(subject.bottle).to eq("強撃ビン")
      end
    end
  end

  describe "#max_kireaji_lv" do
    subject { MHX::SlashAxe.new(name, skill_kireaji_lv: skill_kireaji_lv) }
    let(:skill_kireaji_lv) { 0 }

    context "レギオン、匠0のとき" do
      let(:name) { "レギオン" }

      it "returns max kireaji lv" do
        expect(subject.max_kireaji_lv).to eq(5)
      end
    end

    context "ダイダロス、匠0のとき" do
      let(:name) { "ダイダロス" }

      it "returns max kireaji lv" do
        expect(subject.max_kireaji_lv).to eq(3)
      end
    end

    context "ダイダロス、匠2のとき" do
      let(:name) { "ダイダロス" }
      let(:skill_kireaji_lv) { 2 }

      it "returns max kireaji lv" do
        expect(subject.max_kireaji_lv).to eq(4)
      end
    end
  end

  describe "#element_atk_with_skill" do
    subject { MHX::SlashAxe.new(name) }
    let(:skills) { [:"属性攻撃強化"] }
    let(:element) { :"火" }

    context "無属性なレギオンのとき" do
      let(:name) { "レギオン" }

      it "returns zero" do
        expect(subject.element_atk_with_skill(skills, element)).to eq(0)
      end
    end

    context "火属性なレウス斧のとき" do
      let(:name) { "リオエクスレイズ" }

      context "全属性強化、各属性強化がないとき" do
        let(:skills) { [:"弱点特効"] }

        it "returns original elemental power" do
          expect(subject.element_atk_with_skill(skills, element)).to be_within(0.01).of(35)
        end
      end

      context "全属性強化のみあるとき" do
        let(:skills) { [:"属性攻撃強化", :"水属性攻撃強化+1"] }

        it "returns original elemental power x1.1" do
          expect(subject.element_atk_with_skill(skills, element)).to be_within(0.01).of(38.5)
        end
      end

      context "各属性強化+1のみあるとき" do
        let(:skills) { [:"属性会心", :"火属性攻撃強化+1"] }

        it "returns original elemental power x1.05 + 4" do
          expect(subject.element_atk_with_skill(skills, element)).to be_within(0.01).of(40.75)
        end
      end

      context "全属性強化、各属性強化+2ともにあるとき" do
        let(:skills) { [:"属性攻撃強化", :"火属性攻撃強化+2"] }

        it "returns original elemental power x1.2 + 6" do
          expect(subject.element_atk_with_skill(skills, element)).to be_within(0.01).of(48)
        end
      end
    end
  end
end
