require "yaml"

module MHX
  class ChargeAxe < Weapon
    ATTACK_TYPE = "切断".to_sym
    YAML_FILE_NAME = "charge_axe.yml"
    YAML_DATA = YAML.load File.read(yaml_data_path)

    attr_reader :bottle

    # TODO: @bottle_countを増やして
    # ビンの数を制御できるようにする

    def initialize_by_yaml(yaml_data, skill_kireaji_lv: 0)
      super
      @bottle = yaml_data["装着ビン"]
      self
    end

    def damage(atk: 0, index: i, motion: nil, nikushitsu: {}, skills: [], options: {})
      result = super

      # 属性強化の攻撃力UP
      if options[:"属性強化状態"] && motion.name =~ /^斧/
        if options[:"スタイル"] == "ストライカー"
          result[ATTACK_TYPE] *= 1.2
        else
          result[ATTACK_TYPE] *= 1.15
        end
      end

      # ビンの爆発追加
      # TODO: ビンがある場合のみにする
      # TODO: 多段の場合は最後の攻撃にビン爆発分を追加しているがそれで良いのかの確認
      if motion.options["explode"] && motion.atk.size - 1 == index
        explodes = motion.options["explode"]
        # TODO: 剣の補正は 0.5 : 0
        # TODO: 超高出力の補正は 1 + 0.5 * ビン本数
        pow = options[:"属性強化状態"] ? 1.35 : 1.0

        case @bottle
        when "榴弾ビン"
          if skills.include? :"砲術師"
            pow *= 1.3
          elsif skills.include? :"砲術王"
            pow *= 1.35
          end

          result[:"ビン"] = atk_with_skill(skills) * motion.options["榴弾補正"] * explodes * pow

        when "強属性ビン"
          MHX::ELEMENTS.each do |element|
            next unless result[element]
            result[:"ビン"] = element_atk_with_skill(skills, element) * motion.options["強属性補正"] * explodes * pow * nikushitsu[element].to_f / 100
          end
        end
      end

      result
    end
  end
end
