module MHX
  module DamageSimulator
    def self.simulate(weapons: nil, actions: [], skills: [], items: [], options: {})
      fail "no weapons" unless weapons

      weapon_classes = weapons.map(&:class).uniq
      fail "cannot compare different weapon types" if weapon_classes.size != 1
      weapon_class = weapon_classes.first

      # string actions => class
      actions = actions.map do |a|
        {
          motion: a[:motion].is_a?(MHX::Motion) ? a[:motion] : MHX::Motion.find(a[:motion], weapon_class),
          # TODO: MHX::Nikushitsu作る？
          nikushitsu: a[:nikushitsu],
        }
      end

      weapons.map do |weapon|
        actions.map { |action|
          damages = weapon.damages(
            motion: action[:motion],
            nikushitsu: action[:nikushitsu],
            skills: skills,
            options: options,
            #minus_kireaji: -2,
          )
          damages.map.with_index do |damage, i|
            {
              index: i,
              motion: action[:motion],
              nikushitsu: action[:nikushitsu],
              weapon: weapon,
              skills: skills,
              options: options,
              damage: damage.merge({
                "合計": damage.values.inject(:+),
              }),
            }
          end
        }.flatten
      end
    end

    def self.sort_and_print(results)
      print(results.sort_by { |r| - r.inject(0) { |sum, rr| sum += rr[:damage][:合計] } })
    end

    def self.print(results)
      results.each do |result|
        total_damage = result.map { |r| r[:damage][:"合計"] }.inject(:+)
        puts [
          result.first[:weapon].name,
          result.first[:weapon].respond_to?(:bottle) ? result.first[:weapon].bottle : nil,
          result.first[:skills].join(" "),
          total_damage,
        ].compact.join(", ")
      end
    end
  end
end
