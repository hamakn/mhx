require "yaml"

module MHX
  class SwordAndShield < Weapon
    ATTACK_TYPE = "切断".to_sym
    YAML_FILE_NAME = "sword_and_shield.yml"
    YAML_DATA = YAML.load File.read(yaml_data_path)
  end

  SnS = SwordAndShield
end
