require "yaml"

module MHX
  class SlashAxe < Weapon
    ATTACK_TYPE = "切断".to_sym
    YAML_FILE_NAME = "slash_axe.yml"
    YAML_DATA = YAML.load File.read(yaml_data_path)

    attr_reader :bottle

    def initialize_by_yaml(yaml_data, skill_kireaji_lv: 0)
      super
      @bottle = yaml_data["装着ビン"]
      self
    end

    # エネルギーチャージ3前提なので+30%する
    # TODO: 選択可能に
    def critical_percent(skills, ext: 0)
      super(skills, ext: 30)
    end

    def damage(atk: 0, index: 0, motion: nil, nikushitsu: {}, skills: [], options: {})
      result = super

      # ビン反映
      # TODO: 強撃、強属性以外のビン
      case @bottle
      when "強撃ビン"
        result[ATTACK_TYPE] *= 1.2

      when "強属性ビン"
        MHX::ELEMENTS.each do |element|
          next unless result[element]
          result[element] *= 1.2
        end
      end

      if options[:"剣鬼3"]
        case @bottle
        when "強撃ビン"
          result[ATTACK_TYPE] *= 1.2

        when "強属性ビン"
          MHX::ELEMENTS.each do |element|
            next unless result[element]
            result[element] *= 1.2
          end
        end
      end

      result
    end
  end
end
