module MHX
  class Weapon
    # 斬れ味補正
    # https://gamy.jp/monsterhunter-x/mhx-damage-keisan
    KIREAJI_LV_TO_POWER = {
      0 => 0.5,  # 赤
      1 => 0.75, # 橙
      2 => 1.0,  # 黄
      3 => 1.05, # 緑
      4 => 1.2,  # 青
      5 => 1.32, # 白
      6 => 0,
      7 => 0,
    }
    KIREAJI_LV_TO_ELEMENTAL_POWER = {
      0 => 0.25,   # 赤
      1 => 0.5,    # 橙
      2 => 0.75,   # 黄
      3 => 1.0,    # 緑
      4 => 1.0625, # 青
      5 => 1.125,  # 白
      6 => 0,
      7 => 0,
    }
    # 斬り方補正、値は暫定
    KIREAJI_LV_TO_KIRIKATA_HOSEI = {
      0 => 0.7, # 赤
      1 => 0.7, # 橙
      2 => 0.7, # 黄
      3 => 1.0, # 緑
      4 => 1.0, # 青
      5 => 1.0, # 白
      6 => 0,
      7 => 0,
    }

    # 斬れ味について
    # その武器の持つ斬れ味データ: kireaji_gauge
    #   { 0: [1,1,1,1,1,1], 2: [1,1,1,1,1,1] }
    # 現在の匠Lv: skill_kireaji_lv
    #   0
    # 現在の斬れ味位置（斬れ味データを10倍して扱う）: kireaji
    #   25

    attr_reader :name, :atk, :options, :slots
    attr_reader :kireaji_gauge, :skill_kireaji_lv, :kireaji, :max_kireaji

    def self.all(skill_kireaji_lv: 0)
      fail "abstract" unless self::YAML_DATA
      self::YAML_DATA.map do |yaml_data|
        self.new yaml_data["武器名"], skill_kireaji_lv: skill_kireaji_lv
      end
    end

    def self.yaml_data_path
      File.join(File.dirname(__FILE__), "..", "..", "data", "weapons", self::YAML_FILE_NAME)
    end

    def initialize(name, skill_kireaji_lv: 0)
      self.class::YAML_DATA.each do |yaml_weapon|
        if yaml_weapon["武器名"] =~ /#{name}/
          return self.initialize_by_yaml yaml_weapon, skill_kireaji_lv: skill_kireaji_lv
        end
      end

      fail "no weapon found"
    end

    def initialize_by_yaml(yaml_data, skill_kireaji_lv: 0)
      @name = yaml_data["武器名"]
      @atk = yaml_data["攻撃力"]
      @kireaji_gauge = yaml_data["斬れ味"]
      @slots = yaml_data["スロット"]
      @options = yaml_data["性能"]
      @skill_kireaji_lv = skill_kireaji_lv
      # 初期状態では斬れ味は最大にする
      @max_kireaji = @kireaji_gauge[@skill_kireaji_lv][0...-1].inject(:+) * 10
      @kireaji = @max_kireaji
      self
    end

    # 斬れ味の最大Lvを返す
    def max_kireaji_lv
      max_kireaji_lv = 0
      @kireaji_gauge[@skill_kireaji_lv].each_with_index do |kireaji_haba, kireaji_lv|
        break if kireaji_lv == 7  # 黒部分
        max_kireaji_lv = kireaji_lv if kireaji_haba > 0
      end
      max_kireaji_lv
    end

    # 斬れ味の現在Lvを返す
    def kireaji_lv
      kireaji_lv = 0
      current_kireaji = @kireaji
      @kireaji_gauge[@skill_kireaji_lv].each_with_index do |kireaji_haba, kireaji_lv|
        break if kireaji_lv == 7  # 黒部分
        current_kireaji -= kireaji_haba * 10
        return kireaji_lv if current_kireaji <= 0
      end
      fail
    end

    # 会心の発生確率
    def critical_percent(skills, ext: 0)
      if skills.include? :"見切り+3"
        ext += 30
      elsif skills.include? :"見切り+2"
        ext += 20
      elsif skills.include? :"見切り+1"
        ext += 10
      end

      cp = (@options["会心"] || 0) + ext
      return 100 if cp > 100
      return -100 if cp < -100
      cp
    end

    # 研ぐ
    # @kireajiが最大になる
    def togu
      @kireaji = @max_kireaji
    end

    # スキルを加味した攻撃力
    def atk_with_skill(skills)
      ext = 0

      if skills.include? :"鈍器"
        case kireaji_lv
        when 3 # 緑
          ext += 15
        when 2 # 黄
          ext += 25
        when 0..1 # 赤, 橙
          ext += 30
        end
      end

      if skills.include? :"攻撃力UP【大】"
        ext += 20
      elsif skills.include? :"攻撃力UP【中】"
        ext += 15
      elsif skills.include? :"攻撃力UP【小】"
        ext += 10
      end

      atk + ext
    end

    # スキルを加味した属性攻撃力
    def element_atk_with_skill(skills, element)
      return 0 unless @options[element.to_s]

      pow = 1
      ext = 0
      if skills.include? :"属性攻撃強化"
        pow = 1.1
      end

      skills.each do |skill|
        next unless skill =~ /属性攻撃強化/
        next unless skill =~ /^#{element}.*?(\d)$/
        level = $1.to_i
        # NOTE: MHXに各属性強化+3は存在しない
        case level
        when 2
          pow += 0.1
          ext = 6
        when 1
          pow += 0.05
          ext = 4
        end
      end

      @options[element.to_s] * pow + ext
    end

    def damages(motion: nil, nikushitsu: {}, skills: [], options: {})
      fail "no motion" unless motion

      # 単発攻撃と多段攻撃を処理
      #atks = motion.atk.is_a?(Array) ? motion.atk : [motion.atk]
      [motion.atk].flatten.map.with_index do |atk, i|
        damage(atk: atk, index: i, motion: motion, nikushitsu: nikushitsu, skills: skills, options: options)
      end
    end

    def damage(atk: 0, index: 0, motion: nil, nikushitsu: {}, skills: [], options: {})
      result = {}
      # TODO: 超会心
      # NOTE: この計算式でバッドクリティカルもサポートしている
      cp = critical_percent(skills)
      if skills.include? :"弱点特効"
        # 弱点特効
        # optionsで弱点を攻撃できる確率を入れていなければfail
        fail "no option: :弱点攻撃%" unless options[:"弱点攻撃%"]
        critical_percent_for_weekpoint = [100, cp + 50].min
        critical_power = \
          ((100 - cp) * 1.0 + cp * 1.25) / 100 * (100 - options[:"弱点攻撃%"].to_f) / 100 + \
          ((100 - critical_percent_for_weekpoint) * 1.0 + critical_percent_for_weekpoint * 1.25) / 100 * (options[:"弱点攻撃%"].to_f) / 100
      else
        critical_power = ((100 - cp) * 1.0 + cp * 1.25) / 100
      end

      result[self.class::ATTACK_TYPE] = atk_with_skill(skills) * KIREAJI_LV_TO_POWER[kireaji_lv] * KIREAJI_LV_TO_KIRIKATA_HOSEI[kireaji_lv] * critical_power * (nikushitsu[self.class::ATTACK_TYPE] || 0).to_f / 100 * atk / 100

      elemental_powers = MHX::ELEMENTS.each do |element|
        next if !nikushitsu[element] || nikushitsu[element] == 0
        next unless @options[element.to_s]

        # 属性会心
        # TODO: 属性会心でバッドクリティカルが起こるかの調査
        #       現実装ではバッドクリティカルも起こる
        # TODO: 武器種によっては倍率が1.25倍ではなくなるので修正が必要
        #       http://wikiwiki.jp/nenaiko/?%A5%B9%A5%AD%A5%EB%2F%C2%B0%C0%AD%B2%F1%BF%B4
        if skills.include? :"属性会心"
          result[element] = element_atk_with_skill(skills, element) * KIREAJI_LV_TO_ELEMENTAL_POWER[kireaji_lv] * critical_power * (nikushitsu[element]).to_f / 100
        else
          result[element] = element_atk_with_skill(skills, element) * KIREAJI_LV_TO_ELEMENTAL_POWER[kireaji_lv] * (nikushitsu[element]).to_f / 100
        end
      end

      # 斬れ味減衰
      if @name =~ /^叛/
        # 斬れ味回復系を簡易的に再現
        # TODO: ちゃんとした実装
        @kireaji -= 1
      else
        @kireaji -= 2
      end

      result
    end
  end
end
