require_relative "./mhx/weapon"
require_relative "./mhx/sword_and_shield"
require_relative "./mhx/slash_axe"
require_relative "./mhx/charge_axe"
require_relative "./mhx/motion"
require_relative "./mhx/damage_simulator"

module MHX
  ELEMENTS = %i/火 水 雷 氷 龍/
end
